<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/about','PagesController@about');
Route::get('/produits','ProductController@index')->name('products');
Route::get('/search/produits','ProductController@getProducts')->name('search');
Route::get('/produits/{product}', 'ProductController@show')->name('product');
Route::get('/produits/category/{category}', 'ProductController@getPerCategory')->name('productscategory');
Route::get('/offres','PagesController@offres');

