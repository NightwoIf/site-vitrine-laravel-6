<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='copyright' content=''>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title Tag  -->
    <title>@yield('title')</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}">
    <!-- Web Font -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
        rel="stylesheet">

    <!-- StyleSheet -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Fancybox -->
{{--    <link rel="stylesheet" href="{{asset('css/jquery.fancybox.min.css')}}">--}}
<!-- Themify Icons -->
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('css/niceselect.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{asset('css/flex-slider.min.css')}}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{asset('css/owl-carousel.css')}}">
    <!-- Slicknav -->
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}">

    <!-- Eshop StyleSheet -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    @yield('styles')

</head>
<body class="js">
<!-- Preloader -->
<div class="preloader">
    <div class="preloader-inner">
        <div class="preloader-icon">
            <span></span>
            <span></span>
        </div>
    </div>
</div>
<!-- End Preloader -->


<!-- Header -->
<header class="header shop">
    <!-- Topbar -->
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-12">
                    <!-- Top Left -->
                    <div class="top-left">
                        <ul class="list-main">
                            <li><i class="fa fa-whatsapp"></i>Whatsapp :<p
                                    style="display: inline-block;position: relative;top: 1px;"><a
                                        target="_blank"
                                        href="https://wa.me/212661369600?text=Salam+alaikom+li+bgha+chi+haja+mrehba+%21"
                                        style="font-weight: 700;">+212 661-369600</a>
                                </p></li>
                        </ul>
                    </div>
                    <!--/ End Top Left -->
                </div>
                <div class="col-lg-8 col-md-12 col-12">
                    <!-- Top Right -->
                    <div class="right-content">
                        <ul class="list-main">
                            <li><i class="ti-location-pin"></i>°10 - Rue Jakarta 60000 Oujda.</li>
                        </ul>
                    </div>
                    <!-- End Top Right -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Topbar -->
    <div class="middle-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-12">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="/"><img src="{{asset('images/logo.png')}}" alt="logo"></a>
                    </div>
                    <!--/ End Logo -->
                    <!-- Search Form -->
                    <div class="search-top">
                        <div class="top-search"><a href="#"><i class="ti-search"></i></a></div>
                        <!-- Search Form -->
                        <div class="search-top">
                            <form class="search-form" action="/search/produits" role="search">
                                <input type="text" placeholder="Chercher un produit..." name="search">
                                <button value="search" type="submit"><i class="ti-search"></i></button>
                            </form>
                        </div>
                        <!--/ End Search Form -->
                    </div>
                    <!--/ End Search Form -->
                    <div class="mobile-nav"></div>
                </div>
                <div class="col-lg-8 col-md-7 col-12">
                    <div class="search-bar-top">
                        <div class="search-bar">
                            {{--                            <select>
                                                            <option selected="selected">Tous</option>
                                                            <option>watch</option>
                                                            <option>mobile</option>
                                                            <option>kid’s item</option>
                                                        </select>--}}
                            <form action="{{route('search')}}" role="search">
                                {{ csrf_field() }}
                                <input name="search" placeholder="Chercher un produit..." type="search">
                                <button class="btnn"><i class="ti-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-12">
                    <div class="right-bar">
                        <!-- Search Form -->
                        <div class="sinlge-bar">
                            <a href="#" class="single-icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                        <div class="sinlge-bar">
                            <a href="#" class="single-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <!--/ End Shopping Item -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Header Inner -->
    <div class="header-inner">
        <div class="container">
            <div class="cat-nav-head">

                <div class="row">
                    @yield('header')
                    <div class="col-lg-9 col-12">
                        <div class="menu-area">
                            <!-- Main Menu -->
                            <nav class="navbar navbar-expand-lg">
                                <div class="navbar-collapse">
                                    <div class="nav-inner">
                                        <ul class="nav main-menu menu navbar-nav">
                                            <li class="{{ Request::path() === '/' ? 'active' : ''}}">
                                                <a href="/">Accueil</a></li>
                                            <li class="{{ str_contains(url()->current(),'produits') ? 'active' : ''}}">
                                                <a href="/produits">Accessoires</a></li>
                                            <li class="{{ request()->segment(count(request()->segments())) === 'offres' ? 'active' : ''}}">
                                                <a href="/offres">Meilleurs offres</a></li>
                                            <li class="{{ request()->segment(count(request()->segments())) === 'about' ? 'active' : ''}}">
                                                <a href="/about">Nous contacter</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!--/ End Main Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ End Header Inner -->
</header>

@yield('content')
<footer class="footer">
    <!-- Footer Top -->
    <div class="footer-top section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <!-- Single Widget -->
                    <div class="single-footer about">
                        <div class="logo">
                            <a href="/"><img src="{{asset('images/logo2.png')}}" alt="#"></a>
                        </div>

                        <ul>
                            <li><label style="font-weight: 500;margin-right: 3px">Adresse:</label>°10 - Rue Jakarta
                                60000 Oujda.
                            </li>
                            <li><label style="font-weight: 500;margin-right: 3px">Email:</label>accesmedia2014@gmail.com
                            </li>
                            <li><label style="font-weight: 500;margin-right: 3px">Tel:</label><a
                                    target="_blank"
                                    href="https://wa.me/212661369600?text=Salam+alaikom+li+bgha+chi+haja+mrehba+%21"
                                    style="color: #F7941D;font-weight: 700;">+212 661-369600</a></li>
                        </ul>
                    </div>
                    <!-- End Single Widget -->
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <!-- Single Widget -->
                    <div class="single-footer links">
                        <h4>Liens Utiles</h4>
                        <ul>
                            <li><a href="#">À propos</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Nous contacter</a></li>
                        </ul>
                    </div>
                    <!-- End Single Widget -->
                </div>
                {{--                <div class="col-sm-6 col-md-4 col-lg-4">--}}
                {{--                    <!-- Single Widget -->--}}
                {{--                    <div class="single-footer links">--}}
                {{--                        <h4>Service Client</h4>--}}
                {{--                        <ul>--}}
                {{--                            <li>Contacter moi vers le:</li>--}}
                {{--                            <li>0661369600</li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                    <!-- End Single Widget -->--}}
                {{--                </div>--}}
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <!-- Single Widget -->
                    <div class="single-footer social">
                        <h4>Informations</h4>
                        <!-- Single Widget -->
                        <div class="contact">
                            <p>Chark Accessoire et multimédia(CAM) une PME situant à Oujda qui
                                prend en charge la vente ainsi que le montage des accessoires automobile.
                            </p>
                        </div>
                        <!-- End Single Widget -->
                        <ul>
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                    <!-- End Single Widget -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Top -->
    <div class="copyright">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="left">
                            <p>Copyright © 2020 <a href="/">CAM</a> -
                                All Rights Reserved.</p>
                        </div>
                    </div>
                    {{--                    <div class="col-lg-6 col-12">--}}
                    {{--                        <div class="right">--}}
                    {{--                            <img src="{{asset('images/payments.png')}}" alt="#">--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /End Footer Area -->
<!-- Jquery -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery-migrate-3.0.0.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('js/popper.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Slicknav JS -->
<script src="{{asset('js/slicknav.min.js')}}"></script>
<!-- Owl Carousel JS -->
<script src="{{asset('js/owl-carousel.js')}}"></script>
<!-- Magnific Popup JS -->
<script src="{{asset('js/magnific-popup.js')}}"></script>
<!-- Waypoints JS -->
<script src="{{asset('js/waypoints.min.js')}}"></script>
<!-- Countdown JS -->
<script src="{{asset('js/finalcountdown.min.js')}}"></script>
<!-- Nice Select JS -->
<script src="{{asset('js/nicesellect.js')}}"></script>
<!-- Flex Slider JS -->
<script src="{{asset('js/flex-slider.js')}}"></script>
<!-- ScrollUp JS -->
<script src="{{asset('js/scrollup.js')}}"></script>
<!-- Onepage Nav JS -->
<script src="{{asset('js/onepage-nav.min.js')}}"></script>
<!-- Easing JS -->
<script src="{{asset('js/easing.js')}}"></script>
<!-- Active JS -->
<script src="{{asset('js/active.js')}}"></script>

@yield('scripts')
</body>
</html>
