@extends('layout')
@section('title','CAM - Accessoires')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread-inner">
                        <ul class="bread-list">
                            <li><a href="/">Home<i class="ti-arrow-right"></i></a></li>
                            <li class="active"><a href="/produits">Accessoires</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Product Style -->
    <section class="product-area shop-sidebar shop section ">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-12">
                    <div class="shop-sidebar">
                        <!-- Single Widget -->
                        <div class="single-widget category">
                            <h3 class="title">Catégories</h3>
                            @if(!$categories->isEmpty())
                                <ul class="categor-list">
                                    @foreach($categories as $categorie)
                                        <li>
                                            <a href="{{route('productscategory',['category' => $categorie->slug])}}">{{$categorie->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <ul class="categor-list">
                                    <li>Aucune catégorie</li>
                                </ul>
                            @endif
                        </div>
                        <!--/ End Single Widget -->
                        <!-- Shop By Price -->
                        <!-- Single Widget -->
                        <div class="single-widget recent-post">
                            <h3 class="title">Produits récents</h3>
                            <!-- Single Post -->
                            @if(!$latests->isEmpty())
                                @foreach($latests as $latest)
                                    <div class="single-post first">
                                        <div class="image">
                                            <img src="https://via.placeholder.com/75x75"
                                                 alt="/produits/{{$latest->slug}}">
                                        </div>
                                        <div class="content">
                                            <h5><a href="/produits{{$latest->slug}}">{{$latest->name}}</a></h5>
                                            <p class="price">{{$latest->price}}</p>
                                            <ul class="reviews">
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="single-post first">
                                    <p>Aucun Produits</p>
                                </div>
                        @endif
                        <!-- End Single Post -->
                            <!-- End Single Post -->
                        </div>
                        <!--/ End Single Widget -->
                        <!-- Single Widget -->
                        <div class="single-widget category">
                            <h3 class="title">Marques</h3>
                            @if(!$tags->isEmpty())
                                <ul class="categor-list">
                                    @foreach($tags as $tag)
                                        <li><a href="/produits?tag={{$tag->name}}">{{$tag->name}}</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <div class="categor-list">Aucun tag.</div>
                            @endif
                        </div>
                        <!--/ End Single Widget -->
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-12 ">
                    <div class="row">
                        <div class="col-12">
                            <!-- Shop Top -->
                            <div class="shop-top">
                                <div class="shop-shorter">
                                    <div class="single-shorter">
                                        <label>Sort By :</label>
                                        <select onchange="window.location.href=this.options[this.selectedIndex].value;">
                                            <option
                                                value="?sort=name" {{request()->sort === 'name' ? 'selected' : ''}}>
                                                A-Z
                                            </option>
                                            <option
                                                value="?sort=price" {{request()->sort === 'price' ? 'selected' : ''}}>
                                                Prix
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <ul class="view-mode">
                                    <li class="active"><a href="shop-grid.html"><i class="fa fa-th-large"></i></a></li>
                                    <li><a href="shop-list.html"><i class="fa fa-th-list"></i></a></li>
                                </ul>
                            </div>
                            <!--/ End Shop Top -->
                        </div>
                    </div>
                    <div class="row">
                        @if(!$products->isEmpty())
                            @foreach($products as $product)
                                <div class="col-lg-4 col-md-6 col-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="/produits/{{$product->slug}}">
                                                <img class="default-img" src="{{asset('uploads/'.$product->thumbnail)}}"
                                                     alt="{{$product->name}}">
                                                <img class="hover-img" src="{{asset('uploads/'.$product->thumbnail)}}"
                                                     alt="{{$product->name}}">
                                                @if($product->quantity == 0 )
                                                    <span class="out-of-stock">stock épuisé</span>
                                                @endif
                                            </a>
                                            <div class="button-head">
                                                <div class="product-action-2">
                                                    <a title="Add to cart" href="/produits/{{$product->slug}}">Voir</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <h3><a href="product-details.html">{{$product->name}}</a></h3>
                                            <div class="product-price">
                                                @if($product->price_reduced)
                                                    <span class="old">{{$product->price}} Dhs</span>
                                                    <span>{{$product->price_reduced}} Dhs</span>
                                                @else
                                                    <span>{{$product->price}} Dhs</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="single-product">
                                    Aucun produit n'est disponible...
                                </div>
                            </div>
                        @endif


                    </div>
                    <div class="row" style="width: 100%;display: table;">
                        {{$products->appends($_GET)->links()}}
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                                                                                                      aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <!-- Product Slider -->
                            <div class="product-gallery">
                                <div class="quickview-slider-active">
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                </div>
                            </div>
                            <!-- End Product slider -->
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="quickview-content">
                                <h2>Flared Shift Dress</h2>
                                <div class="quickview-ratting-review">
                                    <div class="quickview-ratting-wrap">
                                        <div class="quickview-ratting">
                                            <i class="yellow fa fa-star"></i>
                                            <i class="yellow fa fa-star"></i>
                                            <i class="yellow fa fa-star"></i>
                                            <i class="yellow fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <a href="#"> (1 customer review)</a>
                                    </div>
                                    <div class="quickview-stock">
                                        <span><i class="fa fa-check-circle-o"></i> in stock</span>
                                    </div>
                                </div>
                                <h3>$29.00</h3>
                                <div class="quickview-peragraph">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum
                                        ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui
                                        nemo ipsum numquam.</p>
                                </div>
                                <div class="size">
                                    <div class="row">
                                        <div class="col-lg-6 col-12">
                                            <h5 class="title">Size</h5>
                                            <select>
                                                <option selected="selected">s</option>
                                                <option>m</option>
                                                <option>l</option>
                                                <option>xl</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-12">
                                            <h5 class="title">Color</h5>
                                            <select>
                                                <option selected="selected">orange</option>
                                                <option>purple</option>
                                                <option>black</option>
                                                <option>pink</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="quantity">
                                    <!-- Input Order -->
                                    <div class="input-group">
                                        <div class="button minus">
                                            <button type="button" class="btn btn-primary btn-number" disabled="disabled"
                                                    data-type="minus" data-field="quant[1]">
                                                <i class="ti-minus"></i>
                                            </button>
                                        </div>
                                        <input type="text" name="quant[1]" class="input-number" data-min="1"
                                               data-max="1000" value="1">
                                        <div class="button plus">
                                            <button type="button" class="btn btn-primary btn-number" data-type="plus"
                                                    data-field="quant[1]">
                                                <i class="ti-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!--/ End Input Order -->
                                </div>
                                <div class="add-to-cart">
                                    <a href="#" class="btn">Add to cart</a>
                                    <a href="#" class="btn min"><i class="ti-heart"></i></a>
                                    <a href="#" class="btn min"><i class="fa fa-compress"></i></a>
                                </div>
                                <div class="default-social">
                                    <h4 class="share-now">Share:</h4>
                                    <ul>
                                        <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->
@endsection
@section('scripts')

@endsection
