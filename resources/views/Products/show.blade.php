@extends('layout')
@section('title','CAM - '.$produit->name)
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/xzoom.css')}}" media="all"/>
<link rel="stylesheet" href="{{asset('css/products.css')}}">
<link type="text/css" rel="stylesheet" media="all" href="{{asset('css/jquery.fancybox.css')}}"/>
<script type="text/javascript" src="{{asset('js/xzoom.js')}}"></script>
<script src="{{asset('js/product.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.fancybox.js')}}"></script>
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread-inner">
                        <ul class="bread-list">
                            <li><a href="/">Accueil<i class="ti-arrow-right"></i></a></li>
                            <li><a href="/produits">Accessoires<i class="ti-arrow-right"></i></a></li>
                            <li class="active">{{$produit->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container dark-grey-text mt-5">

        <!--Grid row-->
        <div class="row wow fadeIn">

            <!--Grid column-->
            <div class="col-md-6 mb-4 ">
                <div class="xzoom-container">
                    <img class="xzoom" id="xzoom-fancy" src="{{asset('images/ugmonk-tshirt-1.jpg')}}"
                         xoriginal="{{asset('images/ugmonk-tshirt-1.jpg')}}" style="width: 450px;"/>
                    <div class="xzoom-thumbs">
                        <a href="{{asset('images/ugmonk-tshirt-1.jpg')}}"><img class="xzoom-gallery" width="80"
                                                                               src="{{asset('images/ugmonk-tshirt-1.jpg')}}"
                                                                               xpreview="{{asset('images/ugmonk-tshirt-1.jpg')}}"></a>
                        <a href="{{asset('images/ugmonk-tshirt-2.jpg')}}"><img class="xzoom-gallery" width="80"
                                                                               src="{{asset('images/ugmonk-tshirt-2.jpg')}}"></a>
                        <a href="{{asset('images/ugmonk-tshirt-3.jpg')}}"><img class="xzoom-gallery" width="80"
                                                                               src="{{asset('images/ugmonk-tshirt-3.jpg')}}"></a>
                        <a href="{{asset('images/ugmonk-tshirt-1.jpg')}}"><img class="xzoom-gallery" width="80"
                                                                               src="{{asset('images/ugmonk-tshirt-1.jpg')}}"></a>
                    </div>
                </div>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-6 mb-4">
                <!--Content-->
                <div class="p-4">
                    <div class="mb-3">
                        @if(!$produit->tags->isEmpty())
                            @foreach($produit->tags as $tag)
                                <a href="" class="badge badge-warning">
                                    <span>{{$tag->name}}</span>
                                </a>
                            @endforeach
                        @else
                            <p>Aucun Tag.</p>
                        @endif
                    </div>
                    <h3>{{$produit->name}}</h3>
                    <p class="lead">
              <span class="mr-1">
                <del>{{$produit->price}}Dhs</del>
              </span>
                        <span>{{$produit->price}}Dhs</span>
                    </p>

                    <p class="lead font-weight-bold">Description</p>

                    <p>{!! $produit->description !!}</p>

                </div>
                <!--Content-->

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

        <hr>

        <!--Grid row-->
        <div class="row d-flex justify-content-center wow fadeIn">

            <!--Grid row-->

            <!--Grid row-->

            <!--Grid row-->

        </div>
    </div>
    <!--Main layout-->
@endsection
@section('scripts')

@endsection
