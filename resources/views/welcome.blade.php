@extends ('layout')
@section('title','Chark Accessoires et Multimedias')
@section('header')
    @if(!$categories->isEmpty())
        <div class="col-lg-3">
            <div class="all-category">
                <h3 class="cat-heading"><i class="fa fa-bars" aria-hidden="true"></i>Catégorie</h3>
                <ul class="main-category">
                    @foreach($categories as $categorie)
                        <li>
                            <a href="{{route('productscategory',['category' => $categorie->slug])}}">{{$categorie->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
@endsection
@section ('content')
    <!-- Slider Area -->
    <section class="hero-slider d-sm-none d-lg-block">
        <!-- Single Slider -->
        <div class="single-slider">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-lg-9 offset-lg-3 col-12">
                        <div class="text-inner">
                            <div class="row">
                                <div class="col-lg-7 col-12">
                                    <div class="hero-text">
                                        <h1><span>Collection</span>des accessoires auto:</h1>
                                        <div class="button">
                                            <a href="/produits" class="btn">Voir les produits</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Single Slider -->
    </section>
    <!--/ End Slider Area -->
    <!-- Collections  -->
    <section class="small-banner section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Catégories</h2>
                    </div>
                </div>
                <!-- Single Banner  -->
                @if(!$categories->isEmpty())
                    @foreach($categories as $categorie)
                        <div class="col-lg-4 col-md-4 col-6 collection" d>
                            <div class="single-banner" style="height: 228px;">
                                <img src="{{asset('uploads/'.$categorie->thumbnail)}}" alt="{{$categorie->name}}">
                                <div class="content">
                                    <p>{{$categorie->name}}</p>
                                    <h3>collection<br>{{$categorie->name}} auto</h3>
                                    <a href="produits/category/{{$categorie->slug}}">Découvrir</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-12">
                        <p>Aucune catégorie.</p>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <div class="product-area most-popular section">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Nouveau Arrivage</h2>
                    </div>
                </div>
            </div>
            @if(!$produits->isEmpty())
                <div class="row">
                    <div class="col-12">
                        <div class="owl-carousel popular-slider">
                            <!-- Start Single Product -->
                            @foreach($produits as $produit)
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="produits/{{$produit->slug}}">
                                            <img class="default-img" src="{{asset('uploads/'.$produit->thumbnail)}}"
                                                 alt="{{$produit->name}}">
                                            <img class="hover-img" src="{{asset('uploads/'.$produit->thumbnail)}}"
                                                 alt="{{$produit->name}}">
                                            @if($produit->quantity == 0 )
                                                <span class="out-of-stock">stock épuisé</span>
                                            @endif
                                        </a>
                                    </div>
                                    <div class="product-content">
                                        <h3><a href="produits/{{$produit->slug}}">{{$produit->name}}</a></h3>
                                        <div class="product-price">
                                            @if($produit->price_reduced)
                                                <span class="old">{{$produit->price}} Dhs</span>
                                                <span>{{$produit->price_reduced}} Dhs</span>
                                            @else
                                                <span>{{$produit->price}} Dhs</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                        <!-- End Single Product -->
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-12">
                        <p>Aucun produit.</p>
                    </div>
                </div>

            @endif
        </div>
    </div>
    <!-- End Most Popular Area -->
    <!-- Start Shop Services Area -->
    <section class="shop-services section home">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-lg-4">
                    <!-- Start Single Service -->
                    <div class="single-service">
                        <i class="ti-truck"></i>
                        <h4>Payement à la livraison</h4>
                        <p>Partout au Maroc</p>
                    </div>
                    <!-- End Single Service -->
                </div>
                <div class="col-lg-3 col-md-6 col-lg-4">
                    <!-- Start Single Service -->
                    <div class="single-service">
                        <i class="ti-face-smile"></i>
                        <h4>Satisfaction total</h4>
                        <p>remboursement si necessaire</p>
                    </div>
                    <!-- End Single Service -->
                </div>
                <div class="col-lg-3 col-md-6 col-lg-4">
                    <!-- Start Single Service -->
                    <div class="single-service">
                        <i class="ti-rocket"></i>
                        <h4>Meilleur prix</h4>
                        <p>Avec garantie</p>
                    </div>
                    <!-- End Single Service -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Shop Services Area -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                                                                                                      aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <!-- Product Slider -->
                            <div class="product-gallery">
                                <div class="quickview-slider-active">
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                    <div class="single-slider">
                                        <img src="https://via.placeholder.com/569x528" alt="#">
                                    </div>
                                </div>
                            </div>
                            <!-- End Product slider -->
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="quickview-content">
                                <h2>Flared Shift Dress</h2>
                                <div class="quickview-ratting-review">
                                    <div class="quickview-ratting-wrap">
                                        <div class="quickview-ratting">
                                            <i class="yellow fa fa-star"></i>
                                            <i class="yellow fa fa-star"></i>
                                            <i class="yellow fa fa-star"></i>
                                            <i class="yellow fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <a href="#"> (1 customer review)</a>
                                    </div>
                                    <div class="quickview-stock">
                                        <span><i class="fa fa-check-circle-o"></i> in stock</span>
                                    </div>
                                </div>
                                <h3>$29.00</h3>
                                <div class="quickview-peragraph">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum
                                        ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui
                                        nemo ipsum numquam.</p>
                                </div>
                                <div class="size">
                                    <div class="row">
                                        <div class="col-lg-6 col-12">
                                            <h5 class="title">Size</h5>
                                            <select>
                                                <option selected="selected">s</option>
                                                <option>m</option>
                                                <option>l</option>
                                                <option>xl</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-12">
                                            <h5 class="title">Color</h5>
                                            <select>
                                                <option selected="selected">orange</option>
                                                <option>purple</option>
                                                <option>black</option>
                                                <option>pink</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="quantity">
                                    <!-- Input Order -->
                                    <div class="input-group">
                                        <div class="button minus">
                                            <button type="button" class="btn btn-primary btn-number" disabled="disabled"
                                                    data-type="minus" data-field="quant[1]">
                                                <i class="ti-minus"></i>
                                            </button>
                                        </div>
                                        <input type="text" name="quant[1]" class="input-number" data-min="1"
                                               data-max="1000" value="1">
                                        <div class="button plus">
                                            <button type="button" class="btn btn-primary btn-number" data-type="plus"
                                                    data-field="quant[1]">
                                                <i class="ti-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!--/ End Input Order -->
                                </div>
                                <div class="add-to-cart">
                                    <a href="#" class="btn">Add to cart</a>
                                    <a href="#" class="btn min"><i class="ti-heart"></i></a>
                                    <a href="#" class="btn min"><i class="fa fa-compress"></i></a>
                                </div>
                                <div class="default-social">
                                    <h4 class="share-now">Share:</h4>
                                    <ul>
                                        <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->
@endsection
@section('scripts')
    <script src="{{asset('js/welcome.js')}}"></script>
@endsection
