<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = [
        'name',
        'thumbnail',
        'category_id'
    ];

    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    //
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products()
    {
        return $this->hasMany(Product::class);

    }
    public function subCategories()
    {
        return $this->hasMany(Category::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function setThumbnailAttribute($value)
    {
        $attribute_name = "thumbnail";
        $disk = "uploads";
        $destination_path = "/categories";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
}
