<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'image_path',
    ];
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    //
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
