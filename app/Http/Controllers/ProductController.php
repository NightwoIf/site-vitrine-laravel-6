<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::whereNotNull('published_at');
        if (\request('search')) {
            Builder::macro('whereLike', function ($attributes, string $searchTerm) {
                $this->where(function (Builder $query) use ($attributes, $searchTerm) {
                    foreach (array_wrap($attributes) as $attribute) {
                        $query->when(
                            str_contains($attribute, '.'),
                            function (Builder $query) use ($attribute, $searchTerm) {
                                list($relationName, $relationAttribute) = explode('.', $attribute);

                                $query->orWhereHas($relationName, function (Builder $query) use ($relationAttribute, $searchTerm) {
                                    $query->where($relationAttribute, 'LIKE', "%{$searchTerm}%");
                                });
                            },
                            function (Builder $query) use ($attribute, $searchTerm) {
                                $query->orWhere($attribute, 'LIKE', "%{$searchTerm}%");
                            }
                        );
                    }
                });

                return $this;
            });
            $products->whereLike(['name', 'categories.name'], \request('search'));
        }
        if (\request('tag')) {
            $products->where('name', \request('tag'));
        }
        if (\request('sort')) {
            if (\request('sort') == 'name') {
                $products->orderBy('name', 'asc');
            } else {
                $products->orderBy('price', 'asc');
            }
        }

        return view('Products.index', [
            'products' => $products->simplePaginate(9),
            'categories' => Category::all(),
            'latests' => Product::whereNotNull('published_at')->latest()->take(5)->get(),
            'tags' => Tag::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
        return view('Products.show', [
            'produit' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function getProducts(Request $request)
    {
        $q = $request->input('search');

        $products = Product::where('name', 'LIKE', '%' . $q . '%', 'AND')
            ->whereNotNull('published_at');
        return view('Products.index', [
            'products' => $products->simplePaginate(9),
            'categories' => Category::all(),
            'latests' => Product::whereNotNull('published_at')->latest()->take(5)->get(),
            'tags' => Tag::all()
        ]);


    }

    public function getPerCategory(Category $category)
    {
        $products = $category->products();
        return view('Products.index',[
            'products' => $products->simplePaginate(9),
            'categories' => Category::all(),
            'latests' => Product::whereNotNull('published_at')->latest()->take(5)->get(),
            'tags' => Tag::all()
        ]);
    }
}
