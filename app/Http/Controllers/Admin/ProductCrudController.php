<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\ProductRequest;
use App\Product;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Models\Traits\HasUploadFields;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use HasUploadFields;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('product', 'products');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns(['id', 'name', 'description', 'quantity', 'price', 'price_reduced']);
        $this->crud->addColumn([
            'name' => 'thumbnail',
            'type' => 'image',
            'label' => 'Image',
            'prefix' => 'uploads/',
            'width' => '50px',
            'height' => '50px'
        ]);
        $this->crud->addColumn([
            'name' => 'category',
            'type' => 'relationship',
            'label' => 'Category',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Crée le'
        ]);
        $this->crud->addColumn([
            'name' => 'published_at',
            'type' => 'check',
            'label' => 'Active'
        ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Nom Accessoire'
        ]);

        $this->crud->addField([

            'type' => 'select2',
            'label' => 'Category',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'default' => ''
        ]);
        $this->crud->addField([
            'name' => 'quantity',
            'label' => 'Quantité',
            'type' => 'number'
        ]);
        $this->crud->addField([
            'name' => 'description',
            'type' => 'easymde',
            'label' => 'Description de l\'accessoire...'
        ]);
        $this->crud->addField([
            'name' => 'thumbnail',
            'label' => 'Image',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'uploads', // if you store files in the /public folder, please omit this; if you store them in /storage or S3, please specify it;
        ]);
        $this->crud->addField([
            'name'      => 'images',
            'label'     => 'Images',
            'type'      => 'relationship',
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => 'Prix TTC',
            'type' => 'number',
            'prefix' => 'DH'
        ]);
        $this->crud->addField([
            'name' => 'price_reduced',
            'label' => 'Prix réduit',
            'type' => 'number',
            'prefix' => 'DH'
        ]);
        $this->crud->addField([
            'name' => 'published_at',
            'label' => 'Activer',
            'type' => 'checkbox'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function fetchCategory()
    {
        return $this->fetch(Category::class);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumn('id');
        $this->crud->addColumn('name');
        $this->crud->addColumn('description');
        $this->crud->addColumn([
            'name' => 'price',
            'label' => 'Prix',
            'suffix' => ' DH'
        ]);
        $this->crud->addColumn([
            'name' => 'category',
            'type' => 'relationship',
            'label' => 'Category'
        ]);
        $this->crud->addColumn([
            'name' => 'thumbnail',
            'type' => 'image',
            'prefix' => 'uploads/',
            'label' => 'Image',
            'height' => '100px',
            'width' => '100px'
        ]);
        $this->crud->addColumn([
            'name' => 'published_at',
            'label' => 'Active',
            'type' => 'check',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Crée le',
            'type' => 'datetime'
        ]);
        $this->crud->addColumn([
            'name' => 'images',
            'type' => 'images',
            'prefix' => 'uploads/',
            'label' => 'Images',
            'width' => '100px',
            'height' => '100px',
        ]);

    }


}
