<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

class PagesController extends Controller
{
    //
    public function index()
    {

        return view('welcome',[
            'produits' => Product::all(),
            'categories' => Category::all(),

        ]);
    }
    public function about()
    {
        return view('about');
    }

    public function offres()
    {
        return view('offres');
    }
}
