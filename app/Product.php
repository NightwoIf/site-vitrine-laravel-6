<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use function PHPSTORM_META\type;

class Product extends Model
{
    protected $fillable = [
        'name',
        'category_id',
        'description',
        'slug',
        'quantity',
        'thumbnail',
        'price',
        'price_reduced',
        'published_at',
    ];
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    //
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function images(){
        return $this->hasMany(Image::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function getPublishedAtAttribute($value)
    {
        if(!empty($value)){
            return 1;
        }
        return 0;
    }
    public function setPublishedAtAttribute($value)
    {
        if($value === "1")
        {
            $this->attributes['published_at'] = date('Y-m-d H:i:s');
            return;
        }
        $this->attributes['published_at'] = null;
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
    public function setThumbnailAttribute($value)
    {
        $attribute_name = "thumbnail";
        $disk = "uploads";
        $destination_path = "/images";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }
    public static function boot()
    {
        parent::boot();
        static::deleting(function($obj) {
            if (count((array)$obj->photos)) {
                foreach ($obj->photos as $file_path) {
                    \Storage::disk('uploads')->delete($file_path);
                }
            }
        });
    }

}
