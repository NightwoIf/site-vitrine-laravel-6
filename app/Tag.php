<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    //
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
