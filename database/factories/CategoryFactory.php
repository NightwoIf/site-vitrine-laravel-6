<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        //
        'category_id' => null,
        'slug' => \Illuminate\Support\Str::slug($name),
        'name' =>$name,
    ];
});

$factory->state(Category::class,'child', function (){
    return [
        'category_id' => factory(Category::class)->create()->id,
    ];
});
