<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $name = $faker->title;
    return [
        'category_id' =>factory(App\Category::class),
        'slug' => \Illuminate\Support\Str::slug($name),
        'name' => $name,
        'description' => $faker->paragraph(3),
        'image_path' => '',
        'price' => $faker->randomFloat(2,0,8),
        //
    ];
});
